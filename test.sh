#!/bin/bash
set -e
killall iohk-challenge > /dev/null 2>&1 || true

# export DISTRIBUTED_PROCESS_TRACE_CONSOLE=yes
# export DISTRIBUTED_PROCESS_TRACE_FLAGS=d

iohk-challenge slave localhost 8080 &
iohk-challenge slave localhost 8081 &
iohk-challenge slave localhost 8082 &
iohk-challenge slave localhost 8083 &

# ps aux | grep "iohk-challenge slave"

sleep 0.1

echo "starting now"
iohk-challenge master localhost 8079 --with-seed 42 --send-for 5 --wait-for 1
echo "done."


{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Lib
    ( challenge
    ) where

import Control.Distributed.Process
import Control.Distributed.Process.Async
import Control.Distributed.Process.Backend.SimpleLocalnet
import Control.Distributed.Process.Closure
import Control.Distributed.Process.Node (initRemoteTable)
import Control.Distributed.Process.Serializable
import Control.Monad
import Control.Monad.Reader
import Control.Monad.State
import Data.Binary
import Data.List
import Data.Maybe (fromJust)
import Data.Time
import GHC.Generics
import System.Environment (getArgs)
import System.Random

data Config = Config
    { seed :: Int
    , sendtime :: Int
    , waittime :: Int
    } deriving (Show, Eq, Generic)

instance Binary Config

slaveAction :: ProcessId -> Process ()
slaveAction mpid
    -- Send own id to master
 = do
    pid <- getSelfPid
    send mpid pid
    -- Receive the nodes' id's
    allpids <- expect
    -- Receive the config
    conf <- getProvisioned
    -- Make the internal config
    sconf <- slaveConf conf allpids
    -- Start running
    flip evalStateT (initState $ seed conf) $
        flip runReaderT sconf $ do
            sendAlot
            printResult

getProvisioned :: Process Config
getProvisioned = expect

data SlaveConfig = SlaveConfig
    { sendEndTime :: UTCTime
    , waitEndTime :: UTCTime
    , myIndex :: Int
    , allNodes :: [ProcessId]
    } deriving (Show)

slaveConf :: Config -> [ProcessId] -> Process SlaveConfig
slaveConf Config {..} pids = do
    mypid <- getSelfPid
    curtime <- liftIO getCurrentTime
    return
        SlaveConfig
        { sendEndTime = fromIntegral sendtime `addUTCTime` curtime
        , waitEndTime =
              (fromIntegral sendtime + fromIntegral waittime) `addUTCTime`
              curtime
        , allNodes = pids
        , myIndex = fromJust $ mypid `elemIndex` pids -- FIXME handle failure case.
        }

data SlaveState = State
    { msgsReceived :: Int
    , totalValue :: Double
    , curRand :: StdGen
    } deriving (Show)

initState :: Int -> SlaveState
initState seed =
    State {msgsReceived = 0, totalValue = 0, curRand = mkStdGen seed}

type Counter = ReaderT SlaveConfig (StateT SlaveState Process)

liftP :: Process a -> Counter a
liftP = lift . lift

registerMsg :: Double -> Counter ()
registerMsg val =
    modify $ \s ->
        let n = msgsReceived s
        in s {msgsReceived = n + 1, totalValue = fromIntegral n * val}

curtime :: Counter UTCTime
curtime = liftIO getCurrentTime

sendAlot :: Counter ()
sendAlot = go
  where
    go = do
        ctime <- curtime
        endtime <- asks sendEndTime
        if ctime >= endtime
            then return ()
            else do
                oneRound
                go

oneRound :: Counter ()
oneRound = do
    msgsr <- gets msgsReceived
    allpids <- asks allNodes
    let currentSender = allpids !! (msgsr `mod` length allpids)
    mypid <- liftP getSelfPid
    if currentSender == mypid
        then do
            msg <- sendMsg
            let awaitAcks [] = pure ()
                awaitAcks remaining = do
                    ("ack", otherpid) <- liftP expect
                    awaitAcks $ filter (/= otherpid) remaining
            awaitAcks $ filter (/= mypid) allpids
            registerMsg msg
        else do
            msg <- liftP expect
            liftP $ send currentSender ("ack", mypid)
            registerMsg msg

genMsg :: Counter Double
genMsg = do
    rand <- gets curRand
    let tup@(res, rand') = randomR (0, 1) rand
    modify (\s -> s {curRand = rand'})
    if res == 0
        then genMsg
        else return res

sendMsg :: Counter Double
sendMsg = do
    selfpid <- liftP getSelfPid
    others <- filter ((/=) selfpid) <$> asks allNodes
    msg <- genMsg
    forM_ others $ \pid -> liftP $ send pid msg
    pure msg

finalise :: Counter ()
finalise = go
  where
    go = do
        ctime <- curtime
        endtime <- asks waitEndTime
        if ctime >= endtime
            then return ()
            else do
                mmesg <- liftP $ expectTimeout 0
                case mmesg of
                    Nothing -> return ()
                    Just mesg -> do
                        registerMsg mesg
                        go

printResult :: Counter ()
printResult = do
    n <- gets msgsReceived
    val <- gets totalValue
    liftIO $ print (n, val)

remotable ['slaveAction]

slaveActionClosure :: ProcessId -> Closure (Process ())
slaveActionClosure = $(mkClosure 'slaveAction)

slaveActionDict = $(functionTDict 'slaveAction)

master :: Backend -> Config -> [NodeId] -> Process ()
master backend conf slaves
    -- Start slaves
 = do
    mpid <- getSelfPid
    let tasks =
            flip map slaves $ \slave ->
                remoteTask slaveActionDict slave (slaveActionClosure mpid)
    asyncs <- mapM async tasks
    -- Make sure the slaves know about eachother
    pids <- identifySlaves $ length slaves
    -- Make sure the slaves know about the configuration to use
    provisionSlaves pids conf
    -- Wait for asyncs to finish
    forM asyncs $ \asnc -> do
        res <- wait asnc
        unless (res == AsyncDone ()) $ say $ show res
    -- Terminate the slaves when the master terminates (this is optional)
    terminateAllSlaves backend

identifySlaves :: Int -> Process [ProcessId]
identifySlaves nrSlaves
    -- Get slaves' process ids.
 = do
    pids <- sort <$> replicateM nrSlaves expect
    -- Send each slaves the process ids of all ondes.
    forM_ pids $ \pid -> do send pid pids
    return pids

provisionSlaves :: [ProcessId] -> Config -> Process ()
provisionSlaves pids conf = do
    let seeds = randoms $ mkStdGen (seed conf) :: [Int]
    forM_ (zip pids seeds) $ \(pid, pseed) -> do
        let pconf = conf {seed = pseed}
        send pid pconf

challenge :: IO ()
challenge = do
    args <- getArgs
    let rTable = __remoteTable initRemoteTable
    case args of
        ["master", host, port, "--with-seed", sd, "--send-for", st, "--wait-for", wt] -> do
            backend <- initializeBackend host port rTable
            let conf =
                    Config
                    {seed = read sd, sendtime = read st, waittime = read wt}
            startMaster backend $ master backend conf
        ["slave", host, port] -> do
            backend <- initializeBackend host port rTable
            startSlave backend
